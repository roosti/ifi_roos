import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/auth")
public class AuthServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		
		if ( login == null || password == null ) throw new ServletException("no login/password");

		boolean succeed = "admin".equals(login) && "admin".equals(password);
		
		if (succeed) {
			HttpSession session = req.getSession();
			session.setAttribute("login", login);
			resp.sendRedirect("./welcome.jsp");
		} else {
			req.setAttribute("errorMessage", "Login or passward not valid");
			req.getRequestDispatcher("./auth.jsp").forward(req, resp);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		if (req.getParameter("logout") != null) {
			HttpSession session = req.getSession();
			session.invalidate();
			resp.sendRedirect("./auth.jsp");
		} else {
			resp.sendError(500,"Accès Interdit");
		}
	}
}
