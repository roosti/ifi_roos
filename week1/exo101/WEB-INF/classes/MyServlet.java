import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/dist")
public class MyServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		resp.setContentType("text/html");
		Writer out = resp.getWriter();
		out.write("<html><body><h1>Distance</h1>");
		out.write("<form action=\"./dist\" method=\"post\">");
		out.write("<label>Point 1 : </label>");
  		out.write("<input id=\"p1lat\" type=\"text\" name=\"p1lat\" placeholder=\"Latitude\">");
		out.write("<label> , </label>");
		out.write("<input id=\"p1lng\" type=\"text\" name=\"p1lng\" placeholder=\"Longitude\">");
		out.write("<br/><br/>");
		out.write("<label>Point 2 : </label>");
		out.write("<input id=\"p2lat\" type=\"text\" name=\"p2lat\" placeholder=\"Latitude\">");
		out.write("<label> , </label>");
  		out.write("<input id=\"p2lng\" type=\"text\" name=\"p2lng\" placeholder=\"Longitude\">");
		out.write("<br/><br/>");
  		out.write("<input type=\"submit\" value=\"Calculer\">");
		out.write("</form></body></html>");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {

		double p1lat = Double.parseDouble(req.getParameter("p1lat"));
		double p1lng = Double.parseDouble(req.getParameter("p1lng"));
		double p2lat = Double.parseDouble(req.getParameter("p2lat"));
		double p2lng = Double.parseDouble(req.getParameter("p2lng"));

		double R = 6371e3;
		double radP1Lat = Math.toRadians(p1lat);
		double radP2Lat = Math.toRadians(p2lat);
		double diffP2P1Lat = Math.toRadians(p2lat-p1lat);
		double diffP2P1Lng = Math.toRadians(p2lng-p1lng);

		double a = Math.sin(diffP2P1Lat/2) * Math.sin(diffP2P1Lat/2) + Math.cos(radP1Lat) * Math.cos(radP2Lat) * Math.sin(diffP2P1Lng/2) * Math.sin(diffP2P1Lng/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		double distance = (R * c) * 0.001;
		
		java.text.DecimalFormat df = new java.text.DecimalFormat("0.#");

		resp.setContentType("text/html");
		Writer out = resp.getWriter();
		out.write("<html><body><h1>Distance</h1>");
		out.write("<h2>La distance entre les deux points est de : " + df.format(distance) + " Km </h2>");
		out.write("</body></html>");
	}
}
