package fr.eservices.drive.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Perishable extends Article {

	@Temporal(TemporalType.DATE)
	protected Date bestBefore;
	protected String lot;
	
	public Perishable() {}
	
	public Perishable(Date bestBefore, String lot) {
		super();
		this.bestBefore = bestBefore;
		this.lot = lot;
	}

	public Date getBestBefore() {
		return bestBefore;
	}

	public void setBestBefore(Date bestBefore) {
		this.bestBefore = bestBefore;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}
}
