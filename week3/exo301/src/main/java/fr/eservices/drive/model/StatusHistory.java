package fr.eservices.drive.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class StatusHistory {

	@Id
	@GeneratedValue
	protected long id;
	@Temporal(TemporalType.DATE)
	protected Date statusDate;
	
	protected Status status;
	
	public StatusHistory() {}
	
	public StatusHistory(long id, Date statuDate) {
		this.id = id;
		this.statusDate = statuDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
