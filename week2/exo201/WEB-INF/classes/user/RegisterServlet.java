package user;

import java.io.IOException;

import javax.servlet.ServletException;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/register")
public class RegisterServlet extends HttpServlet {
	
	private UserDao userDao;
	
	@Override
	public void init() throws ServletException {
		try {
			this.userDao = new UserDaoSqlite("../../users.db");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		String confirmPassword = req.getParameter("confirmPassword");

		if (!password.equals(confirmPassword)) {
			req.setAttribute("errorMessage", "Passwords are not equals");
			req.getRequestDispatcher("./register.jsp").forward(req, resp);
		}

		User user = new User();
		user.setFirstname(firstname);
		user.setFirstname(lastname);
		user.setFirstname(email);
		
		//User u = this.userDao.findByEmail(email);
		this.userDao.add(user, password);
		//if (u != null) {
			this.userDao.add(user, password);
			//User u = this.userDao.findByEmail(email);
			req.setAttribute("successMessage", "Registration success");
			req.getRequestDispatcher("./register.jsp").forward(req, resp);
			//resp.sendRedirect("./register.jsp");
		/**} else {
			req.setAttribute("errorMessage", "Email already used");
			req.getRequestDispatcher("./register.jsp").forward(req, resp);
		}**/
	}
}
