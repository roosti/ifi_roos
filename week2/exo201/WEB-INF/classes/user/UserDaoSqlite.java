package user;

import java.sql.*;

public class UserDaoSqlite implements UserDao {

	static {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			throw new Error(e);
		}
	}

	protected Connection conn;
	public UserDaoSqlite( String userFilePath ) throws SQLException {

		String jdbcUrl = "jdbc:sqlite:" + userFilePath;
		this.conn = DriverManager.getConnection(jdbcUrl);
	}

	@Override
	public void add(User user, String password) {
		try {
			PreparedStatement pst = this.conn.prepareStatement("insert into users (firstname, lastname, email, password) values (?, ?, ?, ?)");
			pst.setString(1, user.getFirstname());
			pst.setString(2, user.getLastname());
			pst.setString(3, user.getEmail());
			pst.setString(4, password);
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(User user, String password) {
		try {
			PreparedStatement pst = this.conn.prepareStatement("update users set firstname = ?, lastname = ?, email = ?, password = ? where id = ?");
			pst.setString(1, user.getFirstname());
			pst.setString(2, user.getLastname());
			pst.setString(3, user.getEmail());
			pst.setString(4, password);
			pst.setLong(5, user.getId());
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public User find(long id) {
		try {
			PreparedStatement pst = this.conn.prepareStatement("select * from users where id = ?");
			pst.setLong(1, id);
			ResultSet rs = pst.executeQuery();
			rs.next();
			User user = new User();
			user.setId(rs.getLong("id"));
			user.setFirstname(rs.getString("firstname"));
			user.setLastname(rs.getString("lastname"));
			user.setEmail(rs.getString("email"));
			rs.close();
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public User findByEmail(String email) {
		try {
			PreparedStatement pst = this.conn.prepareStatement("select * from users where email = ?");
			pst.setString(1, email);
			ResultSet rs = pst.executeQuery();
			rs.next();
			User user = new User();
			user.setId(rs.getLong("id"));
			user.setFirstname(rs.getString("firstname"));
			user.setLastname(rs.getString("lastname"));
			user.setEmail(rs.getString("email"));
			rs.close();
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public long checkPassword(String email, String password) {
		try {
			PreparedStatement pst = this.conn.prepareStatement("select id, password from users where email = ?");
			pst.setString(1, email);
			ResultSet rs = pst.executeQuery();
			rs.next();
			if (rs.getString("password").equals(password))
				return rs.getLong("id");
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;
	}

	@Override
	public void delete(long id) {
		try {
			PreparedStatement pst = this.conn.prepareStatement("delete from users where id = ?");
			pst.setLong(1, id);
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public long exists(String email) {
		try {
			PreparedStatement pst = this.conn.prepareStatement("select id from users where exists (select id from users where email = ?)");
			pst.setString(1, email);
			ResultSet rs = pst.executeQuery();
			if (rs.next())
				return rs.getLong("id");
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;
	}
}
